// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.liferay.documents.mongo.service;

import com.liferay.documents.mongo.model.MGFile;
import com.liferay.documents.mongo.service.MGFileService;
import java.math.BigInteger;
import java.util.List;

privileged aspect MGFileService_Roo_Service {
    
    public abstract long MGFileService.countAllMGFiles();    
    public abstract void MGFileService.deleteMGFile(MGFile MGFile_);    
    public abstract MGFile MGFileService.findMGFile(BigInteger id);    
    public abstract List<MGFile> MGFileService.findAllMGFiles();    
    public abstract List<MGFile> MGFileService.findMGFileEntries(int firstResult, int maxResults);    
    public abstract void MGFileService.saveMGFile(MGFile MGFile_);    
    public abstract MGFile MGFileService.updateMGFile(MGFile MGFile_);    
}
