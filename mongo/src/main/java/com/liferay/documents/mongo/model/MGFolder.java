package com.liferay.documents.mongo.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.layers.repository.mongo.RooMongoEntity;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooMongoEntity
@RooJson
public class MGFolder {

    private String folderName;

    @NotNull
    private Long ownerId;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<MGFile> files = new HashSet<MGFile>();

    @ManyToOne
    private com.liferay.documents.mongo.model.MGFolder parentFolder;
}
