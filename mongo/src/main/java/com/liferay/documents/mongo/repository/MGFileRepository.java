package com.liferay.documents.mongo.repository;

import com.liferay.documents.mongo.model.MGFile;
import java.util.List;
import org.springframework.roo.addon.layers.repository.mongo.RooMongoRepository;

@RooMongoRepository(domainType = MGFile.class)
public interface MGFileRepository {

    List<com.liferay.documents.mongo.model.MGFile> findAll();
}
