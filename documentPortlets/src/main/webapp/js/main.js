
    

AUI().ready('app', 'aui-toolbar', 'aui-tree-view', 'liferay-portlet-url', function (Y) {
    Y.log("Initing app");
    
    var resourceURL = Liferay.PortletURL.createResourceURL(); 
    resourceURL.setResourceId('test');
    resourceURL.setPortletId('documentPortlets_WAR_documentPortlets_INSTANCE_g8ZT');
    
    console.log("---portlet url---");
    console.log(resourceURL);
    
    var toolbar = new Y.Toolbar({
    	contentBox : ".toolbarLayout", 
        children : [
                   {icon: 'plus', label: 'Folder', handler : function(){ Y.log("add folder"); } },
                   {icon: 'plus', label: 'File', handler : function(){ Y.log("add file"); } },
                   {icon: 'minus', label: 'Delete', handler : function(){ Y.log("remove item"); }}
               ]
    });
    
    toolbar.render();
    
    
    var rootNodeIO = new Y.TreeNodeIO({
        label: 'Folders',
        cache: false,
        io: {
            url: resourceURL.toString()
        }
    });
    
    Y.log(rootNodeIO);
    
    var treeView = new Y.TreeViewDD({
    	contentBox : "#treeView",
    	children : [
    	       rootNodeIO ,
    	      { label : 'folder1' },
    	      { label : 'folder2', draggable: true },
    	]
    });

    treeView.after('lastSelectedChange', function(ev){
    	var theNode = ev.newVal;
    	var label = theNode.get('label');
    	Y.log(Y.one("#rightContent"));
    	Y.one("#rightContent").setContent(label);
    });
    
    treeView.render();
	
});