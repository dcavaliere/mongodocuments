// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.liferay.documents.mongo.model;

import com.liferay.documents.mongo.model.MGFolder;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

privileged aspect MGFolder_Roo_Json {
    
    public String MGFolder.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static MGFolder MGFolder.fromJsonToMGFolder(String json) {
        return new JSONDeserializer<MGFolder>().use(null, MGFolder.class).deserialize(json);
    }
    
    public static String MGFolder.toJsonArray(Collection<MGFolder> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<MGFolder> MGFolder.fromJsonArrayToMGFolders(String json) {
        return new JSONDeserializer<List<MGFolder>>().use(null, ArrayList.class).use("values", MGFolder.class).deserialize(json);
    }
    
}
