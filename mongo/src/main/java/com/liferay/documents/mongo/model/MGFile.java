package com.liferay.documents.mongo.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.layers.repository.mongo.RooMongoEntity;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooMongoEntity
@RooJson
public class MGFile {

    private String filename;

    private long ownerId;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<MGFolder> mgFolders = new HashSet<MGFolder>();
}
