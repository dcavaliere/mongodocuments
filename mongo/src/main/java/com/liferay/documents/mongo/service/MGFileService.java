package com.liferay.documents.mongo.service;

import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { com.liferay.documents.mongo.model.MGFile.class })
public interface MGFileService {
}
