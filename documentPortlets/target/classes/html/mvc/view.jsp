<%
/**
 * Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ include file="../init.jsp" %>

<portlet:defineObjects />

<liferay-ui:header title="${user.fullName }" />

<aui:layout cssClass="toolbarLayout">

</aui:layout>

<aui:layout cssClass="mainLayout">
	
	<aui:column columnWidth="20"> 
		<div id='treeView'></div>
	</aui:column>

	<aui:column columnWidth="80">
		<div id="rightContent"></div>
	</aui:column>

</aui:layout>

<portlet:resourceURL var="testResource" id="test"/>
<br/>
<aui:a href="<%= testResource %>" target="_blank" label="testLink"/>


<script>

</script>
