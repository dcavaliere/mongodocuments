// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.liferay.documents.mongo.repository;

import com.liferay.documents.mongo.model.MGFile;
import com.liferay.documents.mongo.repository.MGFileRepository;
import java.math.BigInteger;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

privileged aspect MGFileRepository_Roo_Mongo_Repository {
    
    declare parents: MGFileRepository extends PagingAndSortingRepository<MGFile, BigInteger>;
    
    declare @type: MGFileRepository: @Repository;
    
}
