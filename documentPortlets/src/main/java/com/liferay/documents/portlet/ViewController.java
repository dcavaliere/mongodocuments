package com.liferay.documents.portlet;

import java.util.List;
import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.documents.mongo.model.MGFile;
import com.liferay.documents.mongo.repository.MGFileRepository;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;

@Controller
@RequestMapping("VIEW")
@Configurable(preConstruction=true)
public class ViewController {
	private static final Logger _log = LoggerFactory.getLogger(ViewController.class);
	private String[] values;
	
	@Autowired MGFileRepository docService;
	private List<MGFile> docs;
	
	@RenderMapping
	public String view(RenderRequest request, RenderResponse response, Locale locale, Model model) {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();
		
		model.addAttribute("user", user);
		
		
		
		_log.debug("serving view.jsp");
		values = new String[2];
		values[0] = "hello";
		values[1] = "world";
		docs = docService.findAll();
		
		_log.debug(docService.getClass().getName().toString());
		model.addAttribute("values", values);
		model.addAttribute("docs",docs);
		
		return "view";
	}

	@ResourceMapping(value="test")
	public void test(ResourceRequest request, ResourceResponse response) {
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONObject json = JSONFactoryUtil.createJSONObject();
		try {
			/*ThemeDisplay themeDisplay = (ThemeDisplay) request
					.getAttribute(WebKeys.THEME_DISPLAY);
			User user = themeDisplay.getUser();
			json.put("firstName", user != null ? user.getFirstName() : "");
			json.put("lastName", user != null ? user.getLastName() : "");*/
			
			response.setCharacterEncoding("UTF-8");
			json.put("label", "rootNode");
			json.put("label", "folderA");
			jsonArray.put(json);
			response.getWriter().write(jsonArray.toString());
		} catch (Exception e) {
		}
	}
}
